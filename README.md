# Teste Qranio


## Back-end

### Package installation

Install Yarn:
Open (https://yarnpkg.com/).

Run terminal code
```
$ yarn install
```

### Run server node

Run terminal code
```
$ yarn develop
```

## Mobile

Mobile part of the real-time Twitter sample application using **React Native**.

### Package installation

Install Yarn:
Open (https://yarnpkg.com/).

Run terminal code:
```
$ yarn install
```

### Run app in IOS

Run terminal code:
```
$ react-native run-ios
```

