import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Login from '~/pages/Login';
import Signup from '~/pages/Signup';
import Menu from '~/pages/Menu';
import MarvelList from '~/pages/Marvel/list';
import MarvelDetail from '~/pages/Marvel/detail';

const Routes = createAppContainer(
  createSwitchNavigator ({
    Login,
    Signup: {
      screen: Signup,
      navigationOptions: {
        headerShown: false
      }
    },
      App: createStackNavigator({
        Menu: {
          screen: Menu,
          navigationOptions: {
            headerShown: false
          }
        },
        MarvelList: {
          screen: MarvelList
        },
        MarvelDetail: {
          screen: MarvelDetail
        },
      }),
  })
);

export default Routes;
