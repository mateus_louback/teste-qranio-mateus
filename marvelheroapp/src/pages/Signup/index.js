import React from 'react';
import axios from 'axios';

import {
  Text, Image, StyleSheet, Dimensions,
  View, StatusBar, TouchableOpacity,
  SafeAreaView
} from 'react-native';

import FormSignup from './form'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#0b0a0d'
  },
  safeArea: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    height: Dimensions.get('window').height * 0.11,
    width: Dimensions.get('window').height * 0.11 * (1950 / 662),
  },
  title: {
    color: '#fff',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  back: {
    height: 40,
    width: 40,
    position: 'absolute',
    top: 20,
    left: 0
  }
});

export default function Home({ navigation: { navigate } }) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#0b0a0d" />
      <SafeAreaView style={styles.safeArea}>
        <TouchableOpacity style={styles.back} onPress={() => navigate('Login')}>
          <Image  source={require('../../images/back-icon.png')}
            style={styles.back}
            resizeMode="contain" />
        </TouchableOpacity>
        <Image
          source={require('../../images/logo.png')}
          style={styles.logo}
          resizeMode="contain"
        />

        <Text style={styles.title}>Cadastre-se</Text>

        <FormSignup />
      </SafeAreaView>
    </View>
  );
}
