import React from 'react';

import {
  Text, Image, StyleSheet,
  View, StatusBar, TouchableOpacity,
  SafeAreaView, ImageBackground,
  ScrollView, AsyncStorage
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#0b0a0d'
  },
  scrollView: {
    flex: 2,
    width: '100%',
  },
  safeArea: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardTouchable: {
    width: '100%',
    height: 204,
    borderWidth: 2,
    borderColor: '#bfbfbf',
    borderRadius: 10,
    marginTop: 15,
    marginBottom: 15
  },
  card: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoMarvel: {
    height: 60,
    resizeMode: 'contain',
  },
  userIcon: {
    height: 50,
    width: 50,
    resizeMode: 'contain',
  },
  textUser: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: 'bold',
    width: 'auto',
    height: 50,
    marginTop: 10,
    marginRight: 7
  },
  inline: {
    right: 0,
    top: 30,
    height: 50,
    flexDirection: 'row',
    width: 'auto',
    marginBottom: 50,
    alignSelf: 'flex-end'
  }
});

export default function Menu({ navigation: { navigate } }) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#0b0a0d" />
      <SafeAreaView style={styles.safeArea}>
        <TouchableOpacity style={styles.inline}>
          <Text style={styles.textUser} >{AsyncStorage.getItem('@qranioUser:username')}</Text>
          <Image style={styles.userIcon} source={require('../../images/user.png')} />
        </TouchableOpacity>

        <ScrollView style={styles.scrollView}>
          <TouchableOpacity style={styles.cardTouchable} onPress={() => navigate('MarvelList')}>
            <ImageBackground source={require('../../images/marvel-bg.jpg')}
              style={styles.card} imageStyle={{ borderRadius: 25 }}>
              <Image style={styles.logoMarvel}  source={require('../../images/marvel-logo.png')} />
            </ImageBackground>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
