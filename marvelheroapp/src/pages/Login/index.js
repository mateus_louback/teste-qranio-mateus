import React from 'react';

import {
  Text, Image, StyleSheet, Dimensions,
  View, StatusBar, TextInput, TouchableOpacity
} from 'react-native';

import FormSignin from './form'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#0b0a0d'
  },
  logo: {
    height: Dimensions.get('window').height * 0.11,
    width: Dimensions.get('window').height * 0.11 * (1950 / 662),
  },
  title: {
    color: '#fff',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  boxForm: {
    width: '100%',
    padding: 15,
    backgroundColor: '#19181f',
    borderRadius: 2,
    marginTop: 30
  },
  inputDefault: {
    backgroundColor: '#25242c',
    color: '#FFF',
    height: 50,
    padding: 15,
    fontSize: 16,
    borderRadius: 4,
    marginBottom: 10,
    marginTop: 10
  },
  buttonDefault: {
    height: 50,
    backgroundColor: '#f2cb12',
    borderRadius: 4,
    marginBottom: 10,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButtonDefault: {
    color: '#19181f',
    fontSize: 16,
  },
  textFooter: {
    color: '#bfbfbf',
    marginTop: 25,
    fontSize: 16
  },
  textFooterLink: {
    color: '#FFF',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 25,
    marginLeft: 7
  },
  textInline: {
    flexDirection: 'row'
  }
});

export default function Login({ navigation: { navigate } }) {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#0b0a0d" />
      <Image
        source={require('../../images/logo.png')}
        style={styles.logo}
        resizeMode="contain"
      />

      <Text style={styles.title}>Faça o login ou cadastre-se</Text>

      <FormSignin />

      <View style={styles.textInline}>
        <Text style={styles.textFooter}>Não tem Login? </Text>
        <TouchableOpacity onPress={() => navigate('Signup')}><Text style={styles.textFooterLink}>Cadastre-se</Text></TouchableOpacity>
      </View>
    </View>
  );
}
