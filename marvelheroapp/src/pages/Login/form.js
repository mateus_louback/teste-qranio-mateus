import React from 'react';

import { View, StyleSheet, KeyboardAvoidingView, TextInput,
  TouchableOpacity, Text, Alert, AsyncStorage } from 'react-native';

import { withFormik } from 'formik';
import api from '../../services/api';
import { navigate } from '../../services/navigation';

const styles = StyleSheet.create({
  boxForm: {
    width: '100%',
    padding: 15,
    backgroundColor: '#19181f',
    borderRadius: 2,
    marginTop: 30
  },
  inputDefault: {
    backgroundColor: '#25242c',
    color: '#FFF',
    height: 50,
    padding: 15,
    fontSize: 16,
    borderRadius: 4,
    marginBottom: 10,
    marginTop: 10
  },
  buttonDefault: {
    height: 50,
    backgroundColor: '#f2cb12',
    borderRadius: 4,
    marginBottom: 10,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButtonDefault: {
    color: '#19181f',
    fontSize: 16,
    fontWeight: 'bold'
  },
  KeyboardAvoidingView: {
    width: '100%'
  }
});

const Form = props => (
  <KeyboardAvoidingView behavior="position" style={styles.KeyboardAvoidingView}>
    <View style={styles.boxForm}>
      <TextInput
        style={styles.inputDefault}
        placeholderTextColor="#bfbfbf"
        placeholder="Digite seu email"
        onChangeText={text => props.setFieldValue('identifier', text)}
        keyboardType="email-address"
        autoCapitalize="none"
      />
      <TextInput
        style={styles.inputDefault}
        placeholderTextColor="#bfbfbf"
        placeholder="Digite sua senha"
        onChangeText={text => props.setFieldValue('password', text)}
        secureTextEntry
        keyboardType="default"
      />
      <TouchableOpacity style={styles.buttonDefault} onPress={props.handleSubmit}>
        <Text style={styles.textButtonDefault}>Entrar</Text>
      </TouchableOpacity>
    </View>
  </KeyboardAvoidingView>
);

export default withFormik({
  mapPropsToValues: () => ({ identifier: '', password: '' }),

  handleSubmit: (values) => {
    api
      .post('auth/local', values)
      .then((response) => {
        // Handle success.
        AsyncStorage.setItem("@qranioUser:username", response.data.user.username);
        console.log('User token', response.data.jwt);
        navigate('Menu');
      })
      .catch((error) => {
        // Handle error.
        Alert.alert('Erro!', 'Verifique todos os campos e tente novamente.', error);
      });
  },
})(Form);
