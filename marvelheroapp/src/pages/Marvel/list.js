import React from 'react'
import { TouchableOpacity, View, FlatList, Text, Image, SafeAreaView } from 'react-native'
import md5 from 'js-md5'

const PUBLIC_KEY = '30740bc925055b60af2d317cbe27e50b'
const PRIVATE_KEY = '383a3c3a073efc10a2040ff6362f536681d67726'

export default class Home extends React.PureComponent {
    static navigationOptions = {
        title: 'Heróis'
    }

    state = {
        data: []
    }

    async componentDidMount() {
        const timestamp = Number(new Date())
        const hash = md5.create()
        hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY)

        const response = await fetch(`https://gateway.marvel.com/v1/public/characters?ts=${timestamp}&orderBy=name&limit=10&apikey=${PUBLIC_KEY}&hash=${hash.hex()}`)
        const responseJson = await response.json()
        this.setState({data: responseJson.data.results})
    }

    _renderItem = ({item}) => {
        return  (
            <TouchableOpacity onPress={()=>this._onItemPress(item)} style={{flexDirection:'row', padding: 10, alignItems:'center'}}>
                <Image style={{height: 50, width: 50, borderRadius: 25}} source={{uri: `${item.thumbnail.path}.${item.thumbnail.extension}` }} />
                <Text style={{marginLeft: 10}}>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    _onItemPress = (item) => {
        this.props.navigation.navigate('MarvelDetail', {hero: item})
    }

    render() {
        return (
          <SafeAreaView>
            <FlatList
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item) => item.id}
                ItemSeparatorComponent={()=>
                    <View style={{height:1, backgroundColor: '#f7f7f7'}}
                />}
            />
          </SafeAreaView>
        )
    }
}
