import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

const styles = StyleSheet.create({
  loader: {
    backgroundColor: 'rgba(25, 24, 31, .8)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000
  }
});

export default function Loader() {
  return (
    <View style={styles.loader}>
      <ActivityIndicator color="#f2cb12" size="large" />
    </View>
  );
}
