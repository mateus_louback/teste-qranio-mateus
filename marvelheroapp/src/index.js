import React from 'react';

import '~/config/ReactotronConfig';

import Routes from '~/routes';
import { setNavigator } from './services/navigation';

const App = () => <Routes ref={setNavigator} />;

export default App;
